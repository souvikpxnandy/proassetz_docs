"""
@apiDefine AllowAny
@apiHeader (Request Header) {String} Content-type application/json
"""

"""
@apiDefine IsUserAuthenticated
@apiHeader (Request Header) {String} Authorization The authentication token with Bearer padding.(ex. Bearer token)
"""