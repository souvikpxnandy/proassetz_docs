"""
@apiDefine ResponseFor400
@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST
{
    "status":True,
    "message":"message regarding error or reason of request being unsuccessful ..."
    "data": None
}
"""
"""
@apiDefine ResponseFor500
@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR
{
    "status":True,
    "message":"Something went wrong. Our best minds are working on it."
    "data": None
}

"""

"""
@apiDefine TokenExpired
@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}
"""

"""
@apiDefine TokenInvalid
@apiErrorExample token-invalid:
HTTP 401 UNAUTHORIZED
{
    "detail": "Error decoding signature."
}
"""