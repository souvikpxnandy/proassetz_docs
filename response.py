"""
@apiDefine DefaultResponse
@apiSuccess (Response Body) {Boolean}   status      Indicates whether request was successful
@apiSuccess (Response Body) {String}    message     User readable message regarding result of the request
@apiSuccess (Response Body) {Any}     [data]        The output data, if any

"""

"""
@apiDefine DefaultPaginatedResponse
@apiSuccess (Response Body) {Number}    count       Count of total results in array (paginated result)
@apiSuccess (Response Body) {String}    next        Next page link if any, else null
@apiSuccess (Response Body) {String}    previous    Previous page link if any, else null
@apiSuccess (Response Body) {Object[]}  results     List containing objects

"""