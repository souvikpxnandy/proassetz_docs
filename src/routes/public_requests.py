# API: Newsletter Subscription
"""
@apiGroup Support
@apiName newsletter_subscribe
@api {post} /v1/user-email-subscribe/ Newsletter Subscribe
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription  API to activate Newsletter Subscription service



@apiParam (Request Body)       {String}    email   Email-id to be subscribed

@apiParamExample {json} Request-Example:
{
    "email": "john.doe@email.com"
}


@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True,
    "message": "Thank you for subscribing to our Newsletter",
    "data": None
}


"""


# API: Newsletter Unsubscribe
"""
@apiGroup Support
@apiName newsletter_unsubscribe
@api {post} /v1/user-email-subscribe/ Newsletter Unsubscribe
@apiVersion 1.0.0
@apiPermission Anybody 

@apiDescription  API to Unsubscribe the Newsletter service


@apiParam (Request Body)       {String}    email   Email-id to be subscribed    
@apiParam (Request Body)       {String}    token   token provided by user (sent in mail)    

@apiParamExample {json} Request-Example:
{
    "email": "john.doe@email.com",
    "token":"provide token here"
}


@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "message": "You have successfully been unsubscribed from our mailing list. "
               "You will not receive further e-mail or notifications from our end.", 
    "data": None
}

"""


# API: News-feed Public
"""
@apiGroup Support
@apiName news_feeds
@api {get} /v1/news-feed-public/ News-feeds
@apiVersion 1.0.0
@apiPermission Anybody 

@apiDescription API to get news-feeds


@apiSuccessExample Success-Response:
HTTP 200 OK
{
    count: 17637
    next: "https://staging-1.proassetz.com/api/v1/news-feed-public/?page=2"
    previous: null
    results:[
                {   "content": "Test"
                    "created_at": 1551312000
                    "id": 39
                    "title": "Test 2"
                },
    ]
}


"""

# API: Contact Us
"""
@apiGroup Support
@apiName contact_us
@api {post} /v1/contact-us/ Contact us
@apiVersion 1.0.0
@apiPermission Anybody 

@apiDescription API to contact proassetz with a message


@apiParam (Request Body)       {String}    email            Email-id of user   
@apiParam (Request Body)       {String}    name             Name of user    
@apiParam (Request Body)       {String}    company          Company of user    
@apiParam (Request Body)       {String}    phone_number     Phone number of user    
@apiParam (Request Body)       {String}    message          message provided by user    

@apiParamExample {json} Request-Example:
{
    "email": "john.doe@email.com",
    "name":"John Doe",
    "Company":"ABC Company",
    "phone_number":"+919876543210",
    "message":"Test message",
}


@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "message": "We have successfully received your message.", 
    "data": None
}

"""

