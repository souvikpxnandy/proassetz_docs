
"""
@apiGroup Wallet
@apiName wallet Details
@api {POST} /v1/wallet-details/ Wallet Details
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription wallet Details: API used for wallet details for the user.

@apiParam (Request Body)       {String}    coin           Coin symbol

@apiParamExample {json} Request-Example:
{
    "coin":"BTC"
}


@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True,
    "message": "Wallet Details Fetched !",
    "data": {
            applications_balance: "0.00000000"
            available_balance: "0.00000000"
            executed_balance: "0.00000000"
            in_order_balance: "0.00000000"
            mnemonic_key: null
            total_balance: "0.00000000"
            user_wallet_address: "0xD1C6c3406A296D2ce249875790622F8eFcce0740"
            wallet_active_status: true
            deposit_allow: true
            withdrawal_allow: true
            "balance_in_usd": 0
            "balance_in_euro": 0
            "balance_in_inr": 0
    },
    "isException": False
}

@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}

@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}

@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName Withdrawal Request
@api {POST} /v1/wallet-withdrawal-request/ Withdrawal Request
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Withdrawal Request: API used for crypto withdrawal

 

@apiParam (Request Body)       {String}    coin                         Coin symbol
@apiParam (Request Body)       {String}    withdraw_previous_amount     Actual amount of crypto which a user want to
                                                                        withdrawal.
@apiParam (Request Body)       {String}    withdraw_remark              Any remark if a user want give
@apiParam (Request Body)       {String}    withdraw_user_account        In which address user want to send crypto
@apiParam (Request Body)       {String}    mnemonic_key                 This field for only XLM and XRP
@apiParam (Request Body)       {String}    otp_from_user                G-auth otp need pass with this param


@apiParamExample {json} Request-Example:
{
    "coin":"BTC",
    "withdraw_previous_amount":0.23,
    "withdraw_remark":"Test Withdrawal",
    "withdrawal_user_account":"0xD1C6c3406A296D2ce249875790622F8eFcce0740",
    "mnemonic_key":"7517414",
    "otp_from_user":"456100"

}


 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True, "message": "Your withdraw request has been placed successfully.",
    "data": None,
    "isException": False
}


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}

@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName Wallet Status
@api {POST} /v1/wallet-account-status/ Wallet Status
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Wallet Status: API used for wallet status

 

@apiParam (Request Body)       {String}         user_id                      User id of loged in user
@apiParam (Request Body)       {String}         coin                         Coin symbol
@apiParam (Request Body)       {IntegerField}   is_active                    Need 1 for true and 0 false



@apiParamExample {json} Request-Example:
{
    "coin":"BTC",
    "user_id":"56",
    "is_active":1

}


 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
"status": True,
"message": "Wallet successfully freezed !",
"data": None,
"isException": False
}

{
    "status": True,
    "message": "Wallet successfully unfreezed !",
    "data": None,
    "isException": False
}


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}

@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}

@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName User withdraw request list
@api {POST} /v1/wallet-user-withdraw-request-list/ User withdraw request list
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription User withdraw request list: API used for user's withdrawal request list.

 

@apiParam (Request Body)       {String}         coin                         Coin symbol



@apiParamExample {json} Request-Example:
{
    "coin":"BTC"

}


 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True,
    "message": "Successfully crypto transfer.",
    "data":{
                coin: "PLTC"
                dateTime: "2019-03-18T17:48:39.287549"
                first_name: "B Rohit kumar"
                from_account: "0xD1C6c3406A296D2ce249875790622F8eFcce0740"
                hash_id: "0x24f9f2b4d8cffa05a2798b51e14f0a3e8c68a9a83520581538d3cace6d87cf1b"
                id: 275
                in_blockchain: true
                is_cancelled: false
                is_completed: true
                is_pending: false
                is_processing: false
                to_account: "0x62Ea47ebC63196caF6BB6869345d386bC574C9a4"
                total_amount: 47
                withdrawal_amount: 24
                withdrawal_charge: 23
                withdrawal_id: "fb3efcc9671448c2a553892c19b1ff621552931319"
    }, "isException": False
}



@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}


@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName Wallet Fees
@api {POST} /v1/wallet-fees/<COIN> Wallet Fees
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Wallet Fees: API used to save or update the wallet fees. COIN examples BTC

 

@apiParam (Request Body)       {FloatField}         trade_min_price                 trade minimum price
@apiParam (Request Body)       {FloatField}         trade_gst_fees                  trade gst fees
@apiParam (Request Body)       {FloatField}         trade_buy_fee                   trade buy fees
@apiParam (Request Body)       {FloatField}         trade_sell_fee                  trade sell fees
@apiParam (Request Body)       {FloatField}         trade_min_volume                trade minimum volume
@apiParam (Request Body)       {FloatField}         wallet_platform_charge          wallet platform charge
@apiParam (Request Body)       {FloatField}         min_withdraw_limit              withdrawal minimum limit
@apiParam (Request Body)       {FloatField}         maximum_withdrawal              withdrawal maximum limit
@apiParam (Request Body)       {FloatField}         minimum_deposit                 deposit minimum limit
@apiParam (Request Body)       {FloatField}         withdrawal_fees                 withdrawal fees
@apiParam (Request Body)       {FloatField}         crypto_transfer_limit           crypto transfer limit
@apiParam (Request Body)       {ChoiceField}        coin_type                       coin type



@apiParamExample {json} Request-Example:
{
    "trade_min_price":0.00,
    "trade_gst_fees":0.00,
    "trade_buy_fee":0.00,
    "trade_sell_fee":0.00,
    "trade_min_volume":0.00,
    "wallet_platform_charge":0.00,
    "min_withdraw_limit":0.00,
    "maximum_withdrawal":0.00,
    "minimum_deposit":0.00,
    "withdrawal_fees":0.00,
    "crypto_transfer_limit":0.00,
    "coin_type":"BTC

}


 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True, "message": "Fees Successfully Updated !", "data": None
}


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}


@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName Wallet Fees
@api {GET} /v1/wallet-fees/<COIN> Wallet Fees
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Wallet Fees: API used to get wallet fees. COIN examples BTC

 

 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True,
    "data": {
            buy_fees: 0.08
            coin_type: "ETH"
            crypto_transfer_limit: 0
            gst_fees: 0
            maximum_withdrawal_limit: 1
            min_price: 0
            minimum_deposit: 0
            minimum_volume: 0
            minimum_withdrawal_limit: 0.01
            platform_charges: 0
            sell_fees: 0.08
            withdrawal_fees: 0
    }
}


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}


@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName User deposit list
@api {POST} /v1/wallet-deposit-lists/ User deposit list
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription All user wallet details: With this API user can view all deposit list.

 

@apiParam (Request Body)       {CharField}              coin                    Coin symbol
@apiParam (Request Body)       {IntegerField}           user_id                 User id



@apiParamExample {json} Request-Example:
{
    "coin":"BTC",
    "user_id":56

}

 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True,
    "message": "",
    "data": {
        actual_time: "2019-03-28T07:12:52.268502"
        coin: "BTC"
        created_at: "2019-03-28T07:12:52.271236"
        deposit_amount: 0.0001
        from_user: "None"
        id: 259
        in_blockchain: true
        local_hash: "15b80f56ef8ca6c8218eb328c12f972e0cf6f539db39fc8618db3a7619b6377f"
        mnemonic_key: null
        proassetz_id: null
        to_user: "3FNtii4zbjdi15RDByQqVZfE14CUM5K3U6"
        transaction_type: "Deposit"
        user: 74
        user_email: "rkg.104@getnada.com"
        user_name: "Rkg Rohit"
    },
    "isException": False
}

@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}


@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName Crypto to usd update
@api {POST} /v1/wallet-crypto-usd-update/<COIN>/ Crypto to usd update
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription All user wallet details: With this API admin can update crypto to usd. COIN examples BTC

 

@apiParam (Request Body)       {CharField}              coin                    Coin symbol
@apiParam (Request Body)       {FloatField}             percentage              percentage want to manupulate



@apiParamExample {json} Request-Example:
{
    "coin":"BTC",
    "percentage":4.2

}

 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True,
    "message": "USD price updated successfully.",
    "data": None
}


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}


@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName Crypto to usd list
@api {GET} /v1/wallet-crypto-usd-update/<COIN_SYMBOL> Crypto to usd list
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription All user wallet details: With this API admin can get the  crypto to usd list.COIN_SYMBOL examples BTC_INR

 

 

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True,
    "message": "Data fetched successfully !",
    "data": {
                'price_in_usd': 25464,
                'change_percentage': 4.2,
                'coin': BTC
            }
    }


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}


@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""




"""
@apiGroup Wallet
@apiName Withdrawal deposit fees calculation
@api {GET} /v1/total-deposit-withdrawal-fees/ totalfor Withdrawal deposit fees calculation
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription All user wallet details: This API used to calculate withdrawal fees.

 

 

@apiSuccessExample Success-with-data:
HTTP 200 OK

{
    "status": False,
    "data": 254.22
}


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}


@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""

"""
@apiGroup Wallet
@apiName Withdrawal fees in a period
@api {GET} /v1/transaction-fees/ Withdrawal fees in a period
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users
@apiDescription All user wallet details: This API is for show transaction fees for a particular time period.
@apiSuccessExample Success-with-data:
HTTP 200 OK

{
    "status": True,
    "message": "",
    "data": {
        'platform_charges": 0.00,
        "blockchian_charges": 0.00
        },
        "isException": False
}


@apiErrorExample token-expired:
HTTP 401 UNAUTHORIZED
{
    "detail": "Signature has expired."
}

@apiErrorExample Error-Response-400:
HTTP 400 BAD REQUEST


{
    "status": False,
    "message": "Sorry! We have received a bad request.",
    "isException": False, "data": None
}


@apiErrorExample Error-Response-500:
HTTP 500 INTERNAL SERVER ERROR

{
    "status": False,
    "message": "Sorry! We have received a bad request. HTTP {}".format(status),
    "isException": False, "data": None
}

"""
