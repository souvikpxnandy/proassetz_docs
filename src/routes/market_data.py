# API: Ticker data
"""
@apiGroup Market
@apiName public_ticker
@api {get} /v1/public-market-data/<coin_pair>/ 01. Ticker Data
@apiPermission Anybody

@apiDescription Public Market Ticker: Market ticker data, <coin_pair> is the dynamic coin_pair name (eg. BTC_INR , ETH_INR etc.)


@apiSuccessExample Success-Response:
HTTP 200 OK

{
  "message": "Market Details",
  "status": true,
  "data": {
    "percentChange": "0.01969286",
    "ltp_in_euro": "38198.32",
    "ltp_in_usd": "43089.07",
    "execution_type": "3467521.66",
    "change_amount": "2471329369.26346640",
    "lowest_ask": "3467721.55000000",
    "percentChange24hours": "-4.90221977",
    "trd_obj": false,
    "total24hrVolume": "2471329369.26346640",
    "high24hr": "3744091.95000000",
    "ltp": "3467521.66000000",
    "highest_bid": "3464964.45000000",
    "ltp_in_inr": "3467521.66",
    "low24hr": "3392412.50000000"
  }
}


"""

# API: BUY, SELL, TRADE order book
"""
@apiGroup Market
@apiName public_history
@api {get} /v1/public-market-history/<coin_pair>/<BUY|SELL|TRADE>/ 02. Order Books and Trade Book
@apiPermission Anybody

@apiDescription Public Market BUY/SELL/TRADE book
<coin_pair> is the dynamic coin_pair name (eg. BTC_INR , ETH_INR etc.)

@apiParam   {int}    limit    (optional) limit to get limited results, default limit is 50


@apiSuccessExample Success-Response- BUY Order Book:
HTTP 200 OK

{
    "status":true,
    "message":"Market Details",
    "data":[
        {"volume":"1.63119000",
        "amount":"3463400.14000000",
        "last_updated_time":1641456164,
        "id":21055284,
        "users":["VAULD12345"],
        "club_amount":1
        }
    ]
}

@apiSuccessExample Success-Response- SELL Order Book:
HTTP 200 OK

{
    "status":true,
    "message":"Market Details",
    "data":[{"volume":"1.63119000",
        "amount":"3463400.14000000",
        "last_updated_time":1641456164,
        "id":21055284,
        "users":["VAULD12345"],
        "club_amount":1
        }
        ]
}
@apiSuccessExample Success-Response- Trade Book:
HTTP 200 OK

{
  "status": true,
  "data": [
    {
      "preference": "SELL",
      "amount": "3466809.27000000",
      "users": [
        "VAULD",
        "VAULD12345"
      ],
      "ltp_in_inr": "3466809.27",
      "volume": "0.04522000",
      "ltp_in_euro": "38198.32",
      "executed_time": 1641456026,
      "id": 62041803,
      "ltp_in_usd": "43089.07"
    },
    ]
    "message": "Market Details",
    "isException": False
}

"""
