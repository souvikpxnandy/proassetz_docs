# API: SIGN UP
"""
@apiGroup User
@apiName registration
@api {post} /v1/user-registration/ 01. Signup
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription Signup: API used for registering the user.
    Mandatory fields are first_name, last_name, email, password and terms_and_condition.

 

@apiParam (Request Body)       {String}    first_name           First name of user
@apiParam (Request Body)       {String}    last_name            Last name of user
@apiParam (Request Body)       {String}    email                Email of user
@apiParam (Request Body)       {String}    password             Password must contain Password should contain uppercase,
                                                                lowercase letters, numbers and special characters.
                                                                And also must be at least 8 characters long.
@apiParam (Request Body)       {Number}   terms_and_condition   1 for acceptance, 0 for rejecting.
                                                                Must be 1 for registration, else 0

@apiParamExample {json} Request-Example:
{
    "first_name":"John",
    "last_name ":"Doe",
    "email":"john.doe@email.com",
    "password":"Root@123",
    "terms_and_condition": 1
}
 

@apiSuccessExample Success-with-data:
HTTP 201 CREATED
{
    "status":True,
    "message":"Thank you! You have been registered successfully. "
    "data": None
}

 
 
"""

# API: SIGN IN
"""
@apiGroup User
@apiName authentication
@api {post} /v1/user-login/ 06. Sign in
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription Sign in: API used for user sign in. First User will provide email and password. If authentication is 
successful then -
case-I : 1) If user hadn't activated google auth yet send a otp to email.
         2) If user provide valid otp sent to email then provide him the secret-key for google auth in response , 
         else notify user as invalid otp.
         3) If user provide google_otp(API is also taking email_otp for cross verification) then return token in 
         response, else notify user.
case-II: 1) If user had activated google auth before, then API needs email, password and google_otp,
         if authentication successful then return token in response, else notify user.
          
    
 

@apiParam (Request Body)       {String}    email           Email of user 
@apiParam (Request Body)       {String}    password        Password of user
@apiParam (Request Body)       {String}    email_otp       Unless user activates google auth, he/she will 
                                                           receive otp in email. that otp is be provided into this field
@apiParam (Request Body)       {String}    google_otp      As user activates his/her google authentication,
                                                           otp generated in google authenticator is to be provided.

@apiParamExample {json} Request-Example-initial login:
{
    "email":"john.doe@email.com"
    "password":"Root@123"
}
 

@apiSuccessExample login-successful:
HTTP 200 OK
{
    "status": False,
    "message": "Please enter your OTP. We have sent the OTP to your registered email address. ",
    "data": None, "isLoginStatus": True,
    "otp_type": "email"
}

@apiParamExample {json} Request-Example-provide-email_otp:
{
    "email":"john.doe@email.com",
    "password":"Root@123",
    "email_otp": "123456"
}

@apiSuccessExample login-successful-with email-otp:
HTTP 200 OK
{
    "status": True, 
    "message": "Please enter your Google Authenticator OTP.",
    "data": {
            "first_name": "John",
            "last_name": "Doe",
            "secret_key": "otpauth://totp/Secure%20App:alice%40google.com?secret=JBSWY3DPEHPK3PXP&issuer=Secure%20App",
            "raw_secret_key": "JBSWY3DPEHPK3PXP"
            }, "isLoginStatus": True, "otp_type": "google"}

@apiParamExample {json} Request-Example-google_otp:
{
    "email":"john.doe@email.com",
    "password":"Root@123",
    "email_otp": "123456",
    "google_otp": "123456"
}   

@apiSuccessExample login-successful-with email-otp:
HTTP 200 OK
{
    "status": True,
    "message": "Welcome. Go ahead & submit your KYC./Success!",
    "data": {
            "token": token, 
            "first_name": "John,
            "last_name": "Doe",
            "custom_user_id": "PROZ-1234",
            "user_id": 1,
            "google_authenticated": True,
            "profile_picture": "this field will contain profile picture link if user had uploaded his picture in kyc"
                                "else None",
            "kyc_status": "this field will contain current kyc status of user "
    }
}

 
                                       
"""

# API: EMAIL VERIFICATION
"""
@apiGroup User
@apiName email_verification
@api {post} /v1/user-email-verify/ 02. Email Verification-after Sign up
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription Email verification: This class is used to verify emails. Mandatory fields verification_code to mark the 
                email as verified.


 

@apiParam (Request Body)       {String}    verification_code    Verification code sent to email after signup in email
                                                                to verify email 


@apiParamExample {json} Request-Example:
{
    "verification_code":"7DGK7ST99YJEN0I8LFV7IY925ZP0CX49HAXB90E280U24AZU0WDANKR60T11AIKVSBL6NMCSA9D"
}
 

@apiSuccessExample verification-successful:
HTTP 200 OK
{
    "status": True,
    "message": "Thank you! We have successfully verified your email.",
    "data": None
}

 
                                       
"""

# API: RESEND MAIL FOR EMAIL-VERIFICATION AND OTP
"""
@apiGroup User
@apiName email_resend
@api {post} /v1/user-resend-mail/ 03. Resend Email- Verification/otp
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription Resend email: API is used for re-sending emails for verification or for OTP purpose.

 

@apiParam (Request Body)       {String}    email        email-id of user to send mail
@apiParam (Request Body)       {String}    verify_type  email- to resend email verification mail after signup/
                                                        otp- to resend otp in mail

@apiParamExample {json} Request-Example-resend verification mail:
{
    "email":"john.doe@email.com",
    "verify_type": "email"
}

@apiParamExample {json} Request-Example-resend otp mail:
{
    "email":"john.doe@email.com",
    "verify_type": "otp"
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK
{
    "status": True,
    "message": "Please enter your OTP. We have sent the OTP to your registered email address./email sent",
    "data": None
}

 
 

"""

# API: FORGOT PASSWORD- SEND PASSWORD CHANGING LINK
"""
@apiGroup User
@apiName forgot_password
@api {post} /v1/user-reset-password/ 04. Forgot Password - send password changing link
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription Forgot password -password changing link: After clicking on forgot password user will provide a email id, 
if the user had not activated google auth an otp will be sent 
to that email id, else he/she needs to provide google_auth otp.
And then user will provide the email and otp to proceed to change password-
(An link will be sent to user to change password)

 

@apiParam (Request Body)       {String}    email        email-id of user to send mail
@apiParam (Request Body)       {Number}    otp          otp to change password

@apiParamExample {json} Request-Example- provide email:
{
    "email":"john.doe@email.com",
}

@apiParamExample {json} Request-Example- provide email and otp:
{
    "email":"john.doe@email.com",
    "otp": 123456
}

 
@apiSuccess (Response Body) {Boolean}   otp_required      Indicates whether otp required

@apiSuccessExample Success-Response- only email provided(not google authenticated):
HTTP 200 OK
{
    "status": False,
    "message": "An OTP has been sent to your registered email",
    "otp_required": True,
    "data": None
}

@apiSuccessExample Success-Response- only email provided(google authenticated):
HTTP 200 OK
{
    "status": False,
    "message": "Please enter your Google Auth OTP",
    "otp_required": True,
    "data": None
}

@apiSuccessExample Success-Response- email and otp provided:
HTTP 200 OK
{
    "status": True,
    "message": "We have sent a reset password link in your registered email address.",
    "otp_required": False,
    "data": None
}

 
 

"""


# API: FORGOT PASSWORD- VERIFY AND CHANGE PASSWORD
"""
@apiGroup User
@apiName forgot_password_verify
@api {post} /v1/user-reset-password-verify/ 05. Forgot Password - verify and change password
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription Forgot password link verification: Verify OTP received in email and create new password.

 

@apiParam (Request Body)       {String}    code             code sent in email
@apiParam (Request Body)       {String}    new_password     New password provided by user.
                                                            Password must contain Password should contain uppercase,
                                                            lowercase letters, numbers and special characters.
                                                            And also must be at least 8 characters long.

@apiParamExample {json} Request-Example:
{
    "code":"code sent in email",
    "new_password": "Root@1234"
}

 

@apiSuccessExample Success-Response- email and otp provided:
HTTP 200 OK
{
    "status": True,
    "message": "Thank you, we have successfully changed your password.",
    "data": None
}

 
 

"""

# API : USER CHANGE PASSWORD
"""
@apiGroup User
@apiName change_password
@api {post} /v1/user-change-password/ 08. Change Password
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Change password: This API is used to change the password of the currently logged in user.
Mandatory fields are old_password, new_password and google_otp.

 

@apiParam (Request Body)       {String}    old_password     Old password of the user.
@apiParam (Request Body)       {String}    new_password     New password provided by user.
                                                            Password must contain Password should contain uppercase,
                                                            lowercase letters, numbers and special characters.
                                                            And also must be at least 8 characters long.
@apiParam (Request Body)       {String}     google_otp      otp provided by user(generated in google Auth)

@apiParamExample {json} Request-Example:
{
    "old_password": "Root@1234",
    "new_password": "Root@1234",
    "google_otp": "123456"
}

 

@apiSuccessExample Success-Response:
HTTP 201 CREATED
{
    "status": True,
    "message": "Thanks! your password has been changed successfully.",
    "data": None
}

 
 
 
 

"""

# API: USER ACTIVITY
"""
@apiGroup User
@apiName user_activity
@api {get} /v1/user-activity/ 09. User Activity
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription API to get user's login details 

 

 

@apiSuccessExample Success-Response:
HTTP 200 OK
{
    count: 17637
    next: "https://staging-1.proassetz.com/api/v1/user-activity/?page=2"
    previous: null
    results:[
                { "activity_date": 1554878853,
                    "android_version": "Not Detected",
                    "browser_family": "Chrome",
                    "browser_version": "71.0.3578",
                    "device_family": "Other",
                    "ip_address": "122.163.87.142",
                    "ip_city": "Kolkata",
                    "ip_country": "India",
                    "os_version": "Not Detected"
                },
    ]
}

 
 
 
 

"""

# API: USER NOTIFICATION
"""
@apiGroup User
@apiName user_notification
@api {get} /v1/user-notification/ 10. User Notification
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription  API to get user's notifications 

 

 

@apiSuccessExample Success-Response:
HTTP 200 OK
{
    count: 17637
    next: "https://staging-1.proassetz.com/api/v1/user-activity/?page=2"
    previous: null
    results:[
                {   "body": "Sell Order BAT/BTC(1000.00000000 BAT @ 0.00010000) completed."
                            "0.09999000 BTC credited to BTC wallet."
                    "created_at": "2019-04-09T06:26:13.835455"
                    "id": 220162
                    "pop_up_read": false
                    "read_by_user": false
                    "title": "Sell Trade - Successful"
                },
    ]
}

 
 
 
 

"""

# API: USER NOTIFICATION-READ
"""
@apiGroup User
@apiName user_notification_read
@api {post} /v1/user-notification/ 11. User Notification Read all messages
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription API to acknowledge user's notifications as read 

 

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "message": "All Messages read.", 
    "data": None
}


 
 
 
 

"""

# API: USER DELETE ACCOUNT
"""
@apiGroup User
@apiName delete_account
@api {post} /v1/user-delete-account/ 12. User Delete Account
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription  API to delete use's account

 

@apiParam (Request Body)       {String}    google_otp   OTP generated by google authenticator     

@apiParamExample {json} Request-Example:
{
    "google_otp": "123456"
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "message": "Your Account has been successfully deleted!", 
    "data": None
}


 
 
 
 

"""


# API: AUTHORIZE IP DURING SIGN IN
"""
@apiGroup User
@apiName authorize_ip
@api {post} /v1/ip-identity-verify/ 07. Authorize IP -during sign in
@apiVersion 1.0.0
@apiPermission Anybody

@apiDescription API to authorize ip verification during sign in.

 

@apiParam (Request Body)       {String}    whitelist_token  Token sent in email to verify ip  

@apiParamExample {json} Request-Example:
{
    "whitelist_token": "7cdca6e6d17d48bda97a8d2ad11d5b98"
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "message": "Thank you! for verifying your identity.", 
    "data": None
}


 
 
 
 

"""


# API: REFRESH TOKEN
"""
@apiGroup User
@apiName refresh_token
@api {post} /v1/token-refresh/ 13. Refresh Token
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Refresh Token : API to refresh existing token

 

@apiParam (Request Body)       {String}    token  old JWT token 

@apiParamExample {json} Request-Example:
{
    "token": "eJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmlnX2lhdCI6MTU1MjYyNTY0NiwiZXhwIjoxNTUyNjI3NDQ2LCJ1c2VyX2lkIjo3MiwiZW1haWwiOiJya2cuMDA3QGdldG5hZGEuY29tIiwidXNlcm5hbWUiOiJya2cuMDA3QGdldG5hZGEuY29tIn0.Kq07tSVWz9Ix1TdmKs8sZ36H2Jh16kyWHhIrWSkRHFM"
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "token": "eJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmlnX2lhdCI6MTU1MjYyNTY0NiwiZXhwIjoxNTUyNjI3NDQ2LCJ1c2VyX2lkIjo3MiwiZW1haWwiOiJya2cuMDA3QGdldG5hZGEuY29tIiwidXNlcm5hbWUiOiJya2cuMDA3QGdldG5hZGEuY29tIn0.Kq07tSVWz9Ix1TdmKs8sZ36H2Jh16kyWHhIrWSkRHFM"
}


 
 
 
 

"""

# API: VALIDATE USER FROM TOKEN
"""
@apiGroup User
@apiName validate_user_token
@api {post} /v1/validate-user-token/ 14. Validate User Token
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Refresh Token : API to Check whether the user exists or not

 

@apiParam (Request Body)       {String}    email  Email of user

@apiParamExample {json} Request-Example:
{
    "email": "john.doe@email.com"
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "message": "Record Exists ", 
    "data": None

}

@apiErrorExample user-invalid:
HTTP 401 UNAUTHORIZED
{
    "detail": "No Matches Found"
}

 
 
 
 

"""