
"""
@apiGroup FIAT (INR) Account
@apiName Create Account (Mandatory for Indian consumers)
@api {POST/PUT/GET} /v1/user-fiat-request/INR/ 01. Create info and documents for INR Account


@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Initiate INR account.
account number - SAVINGS/CURRENT, 
please make sure the user first updates his info and then can upload the pan_image or cancel_cheque,
can update multiple fields at same time


@apiParamExample {json} Request-Example:
{
    "bank_name": "", 
    "bank_account_number": "", 
    "account_type" : "", 
    "mobile_number": "", 
    "branch_name": "", 
    "upi_address": "", 
    "ifsc_code": "", 
    "pan_number": "", 
    "pan_image": "", 
    "cancel_cheque" : ""
}

@apiSuccessExample Success-with-data:
HTTP 200 OK
{
    "status": True,
    "message": "Wallet Details Fetched !",
    "data": {
            "id": 1,
            "user": "johndoe@email.com",
            "name": "John Doe",
            "bank_name": "XYZ BANK",
            "bank_account_number": "123456",
            "account_type": "Savings",
            "mobile_number": 9876543210,
            "branch_name": "kolkata",
            "upi_address": "john@upi",
            "status": True,
            "is_approved": False,
            "submitted_on": "2022-01-06",
            "approved_on": null,
            "last_rejected_on": null,
            "rejection_message": "",
            "ifsc_code": "15465",
            "pan_number": "XXXX1234",
            "pan_image": "",
            "cancel_cheque": "",
        },
    "isException": False
}

"""


"""
@apiGroup FIAT (INR) Account
@apiName Submit Account
@api {POST/PUT/GET} /v1/user-fiat-request/INR/?q=submit 02. Submit INR Account for verification

@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription check if user had provided all requirements for FIAT account, else notify user with the mising requirements.


@apiSuccessExample Success-with-data:
HTTP 200 OK
{"status": True, "message": "Submitted Successfully", "data": None}

"""

"""
@apiGroup FIAT (INR) Account
@apiName Remove image form INR Document
@api {POST} /v1/user-fiat-remove-image/INR/  03. Remove image
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Remove image from INR account.
key should be pan_image/cancel_cheque

@apiParamExample {json} Request-Example:
{
    "key": "", 
}

@apiSuccessExample Success-with-data:
HTTP 200 OK
{"status": True, "message": "Image Deleted!", "data": None}

"""