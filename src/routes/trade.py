# API: Place Orders
"""
@apiGroup Trade
@apiName place_order
@api {post} /v1/place-order/ 01. Place Order
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Place Order : API to place Order

 

@apiParam (Request Body)       {String}    order_type       choices are BUY/SELL
@apiParam (Request Body)       {String}    order_category   choices are LIMIT/MARKET/STOP_LIMIT
@apiParam (Request Body)       {String}    price            price provided by user
@apiParam (Request Body)       {String}    amount           amount provided by user
@apiParam (Request Body)       {String}    trade_coin       trade-coin name
@apiParam (Request Body)       {String}    pair_coin        pair-coin name
@apiParam (Request Body)       {String}    stop_price       (optional) if order_category is STOP_LIMIT
                                                            then provide stop_price, when LTP reaches to
                                                            the stop_price provided by user the order will be activated.

@apiParamExample {json} Request-Example:
{
    "order_type": "BUY",
    "order_category": "LIMIT",
    "price": 0.001,
    "amount": 10.02,
    "trade_coin": "LTC",
    "pair_coin": "BTC",
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True,
    "message": "Your Order is being Processed.",
    "data": None
}

 
 
 
 

"""

# API: Cancel Orders
"""
@apiGroup Trade
@apiName cancel_order
@api {post} /v1/cancel-order/ 02. Cancel Order
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Place Order : API to cancel Orders

 

@apiParam (Request Body)       {String}    order_id    provide order id(db id) / generated order id to cancel a order
@apiParam (Request Body)       {String}    coin_pair   provide coin_pair name to cancel all orders of that coin/pair.
                                                        if coin_pair = "ALL" provided then all orders will be cancelled.


@apiParamExample {json} Request-cancel-single-order:
{
    "order_id": 100,
}

@apiParamExample {json} Request-cancel-coin-pair-orders:
{
    "coin_pair": "LTC_BTC",
}

@apiParamExample {json} Request-cancel-all-orders:
{
    "coin_pair": "ALL",
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True,
    "message": "Order cancelled.",
    "data": None
}

 
 
 
 

"""

# API: User Open Orders
"""
@apiGroup Trade
@apiName user_open_orders
@api {post} /v1/user-open-orders/ 03. User Open Order
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription User Open Orders : API to view User Open Orders

 

@apiParam (Request Body)       {String}    coin_pair   provide coin_pair name to get open orders of that coin/pair.
                                                    if coin_pair = "ALL" provided then all open orders will be returned.
                                                    




@apiParamExample {json} Request-coin-pair-orders:
{
    "coin_pair": "LTC_BTC",
}

@apiParamExample {json} Request-all-orders:
{
    "coin_pair": "ALL",
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status":true,
    "message":"orders",
    "next":null,
    "count":1,
    "previous":null,
    "data":[{
        "order_details":{
            "fee_details":{
                "total_tax":"0.0000030000000",
                "total_volume":"0.03000300"
                },
            "order_time":1555588258,
            "executed_volume":"0.00000000",
            "generated_order_id":"P-15555882586086",
            "amount":"0.03000000",
            "stop":0.0,
            "pending_volume":"1.00000000",
            "volume":"1.00000000",
            "order_state":"pending",
            "order_status":true,
            "user_site_id":"PROZ-1540532491",
            "trade_fee":"0.01000000",
            "id":290161,
            "coin_pair":"ETH_BTC",
            "order_type":"BUY",
            "last_updated_time":1555588258,
            "vat_charges":"0.00000000",
            "order_category":"LIMIT"
            },
        "id":290161
        }]
}

 
 
 
 


"""

# API: User Trade History
"""
@apiGroup Trade
@apiName user_trade_history
@api {post} /v1/user-trade-history/ 04. User Trade History
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription User Trade History : API to view User Trade History

 

@apiParam (Request Body)       {String}    coin_pair   provide coin_pair name to get open orders of that coin/pair.
                                                    if coin_pair = "ALL" provided then all open orders will be returned.

@apiParam (Request Body)       {String}    from_date    (optional) from date
@apiParam (Request Body)       {String}    to_date      (optional) to date





@apiParamExample {json} Request-coin-pair-orders:
{
    "coin_pair": "LTC_BTC",
}

@apiParamExample {json} Request-all-orders:
{
    "coin_pair": "ALL",
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "count":17925,
    "next":"https://staging-1.proassetz.com/api/v1/user-trade-history/?page=2",
    "previous":null,
    "results":[
                {
                    "amount":"0.03100000",
                    "generated_execution_id":"E-15555933055221",
                    "order_details":[{
                                "fee_details":{
                                    "total_tax":"0.0000031000000",
                                    "total_volume":"0.03100310"
                                    },
                                "order_time":1555576011,
                                "executed_volume":"1.00000000",
                                "generated_order_id":"P-15555760113514",
                                "amount":"0.03100000",
                                "stop":0.0,
                                "pending_volume":"0.00000000",
                                "volume":"1.00000000",
                                "order_state":"executed",
                                "order_status":true,
                                "user_site_id":"PROZ-1540532491",
                                "trade_fee":"0.01000000",
                                "id":290160,
                                "coin_pair":"ETH_BTC",
                                "order_type":"BUY",
                                "last_updated_time":1555593305,
                                "vat_charges":"0.00000000",
                                "order_category":"LIMIT"
                                },
                                {"fee_details":{
                                    "total_tax":"0.0000240000000",
                                    "total_volume":"0.02997600"
                                    },
                                "order_time":1555593302,
                                "executed_volume":"1.00000000",
                                "generated_order_id":"P-15555933029450",
                                "amount":"0.03000000",
                                "stop":0.0,
                                "pending_volume":"0.00000000",
                                "volume":"1.00000000",
                                "order_state":"executed",
                                "order_status":true,
                                "user_site_id":"PROZ-1540532491",
                                "trade_fee":"0.08000000",
                                "id":290162,
                                "coin_pair":"ETH_BTC",
                                "order_type":"SELL",
                                "last_updated_time":1555593305,
                                "vat_charges":"0.00000000",
                                "order_category":"LIMIT"
                                }],
                    "id":33335,
                    "executed_time":1555593305,
                    "volume":"1.00000000"
                }]
  }  

 
 
 
 


"""

# API: Specific Order Details
"""
@apiGroup Trade
@apiName specific_order
@api {get} /v1/user-specific-order/<order_id>/ 05. Show Specific Order details
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Specific Order Details : API to get specific order details

 


 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "isException": False, 
    "message": "Specific Order Details",
    "data": {
        {"fee_details":{
                        "total_tax":"0.0000240000000",
                        "total_volume":"0.02997600"
                        },
        "order_time":1555593302,
        "executed_volume":"1.00000000",
        "generated_order_id":"P-15555933029450",
        "amount":"0.03000000",
        "stop":0.0,
        "pending_volume":"0.00000000",
        "volume":"1.00000000",
        "order_state":"executed",
        "order_status":true,
        "user_site_id":"PROZ-1540532491",
        "trade_fee":"0.08000000",
        "id":290162,
        "coin_pair":"ETH_BTC",
        "order_type":"SELL",
        "last_updated_time":1555593305,
        "vat_charges":"0.00000000",
        "order_category":"LIMIT"
    }
}

 
 
 
 

"""

# API: Make a coin/pair favourite
"""
@apiGroup Trade
@apiName make_favourite_coin_pair
@api {post} /v1/make-favourite-pair/ 06. Make Coin-Pair favourite
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Make Coin-Pair Favourite : API to select/unselect coin-pair as favourite

 
@apiParam (Request Body)        {String}   pair_id             provide pair_id

@apiParamExample {json} Request-Example:
{
    "pair_id": 123
}

 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True, 
    "isException": False, 
    "message": "Added to Favorites",
    "data": None
}

 
 
 
 

"""

# API: View favourite coin/pair
"""
@apiGroup Trade
@apiName view_favourite_coin_pair
@api {get} /v1/view-favourite-pair/ 07. View Favourite Coin-Pairs
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Make Coin-Pair Favourite : API to view favourite coin-pairs

 


 

@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status":true,
    "message":"Favorite pairs",
    "data":{
        "coins_list":[
            {"is_favourite":true,
            "trade_data":{
                "trd_obj":false,
                "lowest_ask":"92.95000000",
                "percentChange24hours":"48.90885750",
                "high24hr":"105.97000000",
                "percentChange":"-0.16137708",
                "change_amount":"103748774.78499894",
                "ltp":"92.80000000",
                "highest_bid":"92.81000000",
                "execution_type":"92.80",
                "total24hrVolume":"1339567.46000000",
                "ltp_in_usd":"0.00",
                "low24hr":"89.61000000"
                },
                "id":123,
                "trade_coin":"BAT",
                "pair_coin":"INR"}
            ]},
        "isException":false
    }
"""

# API: Available coin-pairs list
"""
@apiGroup Trade
@apiName available_pair
@api {get} /v1/view-available-pair/<pair_name>/ 08. Available coin pairs
@apiPermission Anybody

@apiDescription Available Pairs: Available Pair list

@apiParam   {String}    mode    (optional) if mode= onlypairs it will return only pairs name else 
                                ticker data will also be returned

 

@apiSuccessExample Success-Response-onlypairs:
HTTP 200 OK

{
    "status":true,
    "message":"Available pairs",
    "data":{
        "coins_list":[
            {"trade_data":null,
            "trade_coin":"CRYPTO",
            "is_favourite":false,
            "pair_coin":"BTC",
            "id":80
            },
            {"trade_data":null,
            "trade_coin":"BTNT",
            "is_favourite":false,
            "pair_coin":"BTC",
            "id":78
            }]
        }
}

@apiSuccessExample Success-Response-pairs-with-ticker:
HTTP 200 OK

{
  "status": true,
  "message": "Available pairs",
  "data": {
    "pair_coin": "INR",
    "coins_list": [
      {
        "is_favourite": false,
        "trade_data": {
          "execution_type": "SELL",
          "ltp": "80.24000000",
          "percentChange24hours": "0.63965884",
          "high24hr": "80.43000000",
          "percentChange": "0.00000000",
          "total24hrVolume": "1045427.39999999",
          "low24hr": "79.38000000",
          "change_amount": "80937859.95162159"
        },
        "id": 135,
        "pair_coin": "INR",
        "trade_coin": "USDT"
      }
    ]
  },
  "isException": false
}

 
 

"""

# API: User Trade Engine status
"""
@apiGroup Trade
@apiName user_trade_engine_status
@api {get} /v1/user-trade-engine-status/ 09. User Trade Engine status
@apiPermission Anybody

@apiDescription Available Pairs: Available Pair list

 

@apiSuccessExample Success-Response-onlypairs:
HTTP 200 OK

{
    "status":true,
    "message":"All Trade Engine Status Fetched ",
    "data": [
        {
            "engine_status": true,
            "pair_coin": "BTC",
            "trade_coin": "ETH"
        },
    ]
}

 
 

"""