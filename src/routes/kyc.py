# API: GET KYC details
"""
@apiGroup Kyc
@apiName kyc_details_get
@api {get} /v1/update-kyc-details/ 01. Get KYC details
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Get KYC details : API to get kyc details


@apiSuccessExample Success-Response:
HTTP 200 OK

{
    "status": True,
    "message": "Your Order is being Processed.",
    "data": {
                "address": "KolkataABCD"
                "city": "KOLKATA"
                "country": "Kazakhstan"
                "country_code": "+76"
                "custom_user_id": "PROZ-1540532491"
                "dob": "2000-10-25"
                "driving_license_back_image": "https://d34xu0nc31qjhv.cloudfront.net/knowyourcustomer/8e673e8004434241a0f4c491587ca7841540550957.png?Expires=1555670622&Signature=BQYlMOo5MrGX3ds9Y200sDin4rYAONcdKJgjSJMxvsAn62A~-oHVUXCtlp9wuqBAwXA2gd7Zr-~KrNUOGuMh9MHt4AbgdlXEsLWjeiFWJUVhbguCgcX2x2zbP9CinidhZch-Vn2IqEPR5e5iuibkhomqwgeUiXK6~7mA9DI3xaDIdIf5RQGBb9M82yhujOr3PWTY5UbPWjxAFVHZGlOZPK36tNZonnKTHddlB1qpbD29FG-2jTeV~hF2SKcVXCPWmA1-Hno6ExRsSTfDY7SYOuUJRxQtHaEKLR4JD-JRZExSGOZ~3Rde6i1r1gzdvZP-u5dAW5Qq~1EjX~vYF9v6Cw__&Key-Pair-Id=APKAITRDQAJG36OK3DTQ"
                "driving_license_front_image": "https://d34xu0nc31qjhv.cloudfront.net/knowyourcustomer/8a4315b2fd2f4932992e9fd5f925a7971540550951.png?Expires=1555670622&Signature=QGoWX16Elw9lhbma4HOBqG9dHTpJKgl9esffQNlgSmNpTn1ZSwZ3AB1KfHQiYfwphsRQE8s6EsQeRzt5YFMTplC0SutgOfE-P5by~BsKbm7ppqjW58i2X6mogXwMgSI4xrmT51T3gB63w37WRDbL7APmNOIFyt8zmP0HErUNt768VMHXf3s8hWAQNUiK4kw7lbtfkiyjCn69FHvJ8DsWAfjf85yC2zaCrAthFqfGxNRx3mDKdP73UaaNjGZ4Sc~cC9m1LwRC70lJTHyrJN1A1UiJx~lloBc5ghoN8utALQwLYK~ssyOfVwWDDEfzPR4dWoBiR3qME0jzuVNKwTQBSA__&Key-Pair-Id=APKAITRDQAJG36OK3DTQ"
                "email": "rohit.gayakwad@rplanx.com"
                "first_name": "B Rohit kumar"
                "identity_proof_number": "UIYUIYB987845 JUH OIUYKI-90"
                "last_name": "Gayakwad"
                "passport_back_image": null
                "passport_front_image": null
                "person_holding_passport_back": null
                "person_holding_passport_front": "https://d34xu0nc31qjhv.cloudfront.net/knowyourcustomer/7f883cebf9b44523b5d5e40c74ad9ec51540550966.png?Expires=1555670622&Signature=fO8sKEqVq50-LshizPH-~ZSD1-axHP0o-OLcswVvL-tpBct8ijli0fhyGjC9p9bpqcyrRPDhPElkrx0TKx1ilhAc9mPgWZNZpRmVJ7yowf6Ab5a3wvnwKeGrF7ziiOND57eUUDdO737PVRu699Ba6RNeU9CCZyWoich5jG12rr5R6qqN4TOFTmcF~c6~Axe0WZ0qnXus0~snKbjKm1WXC0s4XdZgaJf1AkkXPZthrnpeDoAqN2IM6mEHjyVkv9mFow8mf42cQnXg-ETB-McKkt1WEJ1A9j0yGuI6SUPePa8cI8mNgcm3uQOKkVJJM~h6ud1l5LBXnrGEf-qN6tkmrA__&Key-Pair-Id=APKAITRDQAJG36OK3DTQ"
                "phone_number": "000000001657"
                "postal_code": "4673 57 GGY"
                "state": ""
                "user_profile_image": "https://d34xu0nc31qjhv.cloudfront.net/knowyourcustomer/f0f0bd4261224559a9f94b38442c924f1540550933.png?Expires=1555670622&Signature=gmop3koaBZ8TVLtHx1SV4OkM1GOHJnCWX70uw3B3S9bt51cmtUYYs9luD1siANJdaEioSmTgUDWAG6w4L~uaxvMjrMyjxmO90VIaN0LUTABX~STRHL7V0mDQxjhBXv3lxBJ5eUSWheuiY8Fe4jz6RB30JAS0ZYXYDq3ufDSx0XezYHLbf8H0k2EXLGBBdXnB7fTOSDHwHqzPJgbughLkKZtWVHMIvyYecZaH5sIOVDb5k6Ymoj4kA5vHCUh0Q69ctRlvrldteXBdM96K5eZEtY51aolyvx3DtdIuP-5hQvIuNASNwnImwICh2XVV-CPbfocccz6bDWG~CnlK9vvI6Q__&Key-Pair-Id=APKAITRDQAJG36OK3DTQ"
    }


"""

# API : Update KYC details
"""
@apiGroup Kyc
@apiName kyc_details_update
@api {post} /v1/update-kyc-details/ 02. Update KYC details
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Update KYC details : API to update kyc details


@apiParam (Request Body)       {String}    dob              Date Of Birth (required format YYYY-MM-DD)
@apiParam (Request Body)       {String}    id_number        ID number of user
@apiParam (Request Body)       {String}    country          Select Country from provide list
@apiParam (Request Body)       {String}    phone_number     Phone Number of user
@apiParam (Request Body)       {String}    country_code     Country-code of user
@apiParam (Request Body)       {String}    address          Address of user
@apiParam (Request Body)       {String}    city             City of user
@apiParam (Request Body)       {String}    state            (optional) State of user
@apiParam (Request Body)       {String}    postal_code      (optional) Postal Code of user

@apiParamExample {json} Request-Example:
{
    "dob": "1190-12-01",
    "id_number": "XXXXXXXXXXX",
    "country": "India",
    "phone_number": "9876543210",
    "country_code": "+91",
    "address": "Address",
    "city": "Kolkata"
}


@apiSuccessExample Success-Response:
HTTP 201 CREATED
{
    "status": True, 
    "message": "Thank you! Kindly proceed and upload your KYC Documents.", 
    "data": None
}


"""

# API: Update KYC documents
"""
@apiGroup Kyc
@apiName kyc_documents_update
@api {post} /v1/update-kyc-documents/ 03. Update KYC documents
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Update KYC documents : API to update kyc documents


@apiParam (Form Body)       {File}    driving_license_front_image       Front Size of driving license
@apiParam (Form Body)       {File}    driving_license_back_image        Back Size of driving license
@apiParam (Form Body)       {File}    passport_front_image              Front Size of passport
@apiParam (Form Body)       {File}    passport_back_image               Back Size of passport
@apiParam (Form Body)       {File}    person_holding_passport_front     Person holding Front Size of passport
@apiParam (Form Body)       {File}    person_holding_passport_back      Person holding Back Size of passport
@apiParam (Form Body)       {File}    user_profile_image                User picture
  



@apiSuccessExample Success-Response:
HTTP 200 OK
{
    "status": True, 
    "message": "Data Uploaded Successfully!", 
    "data": None
}

"""

# API: Remove image
"""
@apiGroup Kyc
@apiName remove_image
@api {post} /v1/remove-image/ 04. Remove KYC images
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Remove KYC images : API to remove kyc images


@apiParam (Request Body)       {String}    key   key name to be deleted(ex. driving_license_front_image etc.)

@apiParamExample {json} Request-Example:
{
    "key": "driving_license_front_image"
}



@apiSuccessExample Success-Response:
HTTP 200 OK
{
    "status": True, 
    "message": "Image Deleted!", 
    "data": None
}

"""

# API: Submit KYC
"""
@apiGroup Kyc
@apiName submit_kyc
@api {get} /v1/kyc-status-check/ 05. Submit KYC
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Submit KYC: API to submit KYC


@apiSuccessExample Success-Response:
HTTP 200 OK
{
    "status": True, 
    "message": "Thank you! We have received all your KYC Documents.", 
    "data": None
}


"""

# API:

# API: Submit KYC
"""
@apiGroup Kyc
@apiName kyc_submission_status
@api {get} /v1/kyc-submission-status/ 06. KYC Submission Status
@apiVersion 1.0.0
@apiPermission Authenticated Users/Logged in users

@apiDescription Kyc Submission Status: API to check KYC submission status


@apiSuccessExample Success-Response:
HTTP 200 OK
{
    "status": True, 
    "message": <user's current kyc status> , 
    "data": None
}

"""